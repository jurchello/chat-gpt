@extends('layouts.app')

@section('content')

    <div class="chat-container">
        <div id="with-overflow" class="with-overflow">
            <ul id="dialogue" class="chat"></ul>
        </div>

        <div>
            <input id="question" name="question" type="text" class="text_input" placeholder="Type your question"/>
            <button id="submit-btn" type="submit" class="btn btn-info">Send question</button>
        </div>
    </div>

@endsection()
