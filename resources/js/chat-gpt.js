const LARAVEL_TOKEN = document.querySelector('meta[name="csrf-token"]').textContent;
const SEND_MESSAGE_URL = '/api/send-message-to-gpt'
const GPT_AVATAR_FILE_NAME = "gpt.jpg"
const MY_AVATAR_FILE_NAME = "me.jpg"

$(document).on('click', '#submit-btn', function (e) {
    const questionInput = $('#question')
    appendMessage(MY_AVATAR_FILE_NAME, questionInput.val(), 'left')
    sendMessage()
})

const scrollToBottom = () => {
    let elem = document.getElementById('with-overflow');
    const {scrollHeight, scrollTop} = elem;
    elem.scrollTop = scrollHeight;
}

function appendMessage(avatarFileName, text, classes) {
    const messageItem = appendLiItem(avatarFileName, text, classes);
    $('#dialogue').append(messageItem);
    scrollToBottom()
}

function appendLiItem(avatarFileName, text, classes) {
    return `
    <li class="message ${classes}">
      <img class="logo" src="/img/${avatarFileName}" alt="">
      <p>${text}</p>
    </li>`
}

const clearQuestionInput = (input) => input.val("")

function sendMessage() {
    let questionInput = $('#question');
    let data = {question: questionInput.val()}
    clearQuestionInput(questionInput)
    axios.post(SEND_MESSAGE_URL, data, {
        responseType: 'json',
        headers: {'X-CSRF-TOKEN': LARAVEL_TOKEN}
    }).then(response => {
        appendMessage(GPT_AVATAR_FILE_NAME, response.data.gpt_answer, 'right')
    })
}
