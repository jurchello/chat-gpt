<?php


namespace App\Services\Gpt;


use App\Services\Gpt\Exceptions\CreateResponseException;
use App\Services\Gpt\Exceptions\ReadAnswerException;
use Exception;
use OpenAI\Laravel\Facades\OpenAI;
use OpenAI\Responses\Completions\CreateResponse;

class MessageSender
{
    private const MAX_TOKENS = 50;
    private CreateResponse $response;

    /**
     * @throws CreateResponseException
     */
    public function send(string $question): MessageSender
    {
        try {
            $this->response = OpenAI::completions()->create([
                'model' => config('openai.model'),
                'prompt' => $question,
                'max_tokens' => self::MAX_TOKENS,
            ]);
        } catch (Exception $e) {
            throw new CreateResponseException($e->getMessage(), $e->getCode());
        }

        return $this;
    }

    /**
     * @return string
     * @throws ReadAnswerException
     */
    public function answer(): string
    {
        try {
            $answer = trim((string)$this->response['choices'][0]['text']);
        } catch (Exception $e) {
            throw new ReadAnswerException($e->getMessage(), $e->getCode());
        }

        return $answer;
    }
}
