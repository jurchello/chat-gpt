<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Services\Gpt\MessageSender;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    /**
     * @param Request $request
     * @param MessageSender $messageSender
     * @return JsonResponse
     */
    public function sendMessageToGpt(Request $request, MessageSender $messageSender): JsonResponse
    {
        try {
            return response()->json([
                'success' => true,
                'gpt_answer' => $messageSender->send($request->question)->answer()
            ]);

        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], $e->getCode());
        }
    }
}
