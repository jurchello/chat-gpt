import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/libraries.js',
                'resources/js/custom.js',
            ],
            refresh: true,
        }),
    ],
});
