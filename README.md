[![Watch the video](public/img/demonstration.jpg)](https://www.youtube.com/watch?v=4-VU1ebHavc)

## About the app
This is a small Laravel application for my portfolio. It allows you communicating with **Chat GPT**.
### What I used here
[![Generic badge](https://img.shields.io/badge/Laravel-10->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/PHP-8.1->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/Bootstrap-5.2.3->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/Axios-1.4.0->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/Jquery-3.7.0->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/Vite-4.0.0->.svg)](https://shields.io/)

### The project tags
[![Generic badge](https://img.shields.io/badge/OpenAI->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/Chat_GPT->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/Javascript->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/SCSS->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/CSS->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/HTML->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/JSON->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/Blade->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/Composer->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/NodeJS->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/API_routing->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/Web_routing->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/PHP_code_quality->.svg)](https://shields.io/)
[![Generic badge](https://img.shields.io/badge/Best_practice->.svg)](https://shields.io/)

### I didn't seek to fit all this into one project:
- Vue, Angular, React and others UI frameworks
- Typescript  
- Docker
- sequrity access for API and custom middlewares
- admin panel


## How to install
- Clone the project to the local machine
- Configure the .env file:
    - define *APP_NAME*
    - define *APP_URL*
    - define *OPENAI_API_KEY* and *OPENAI_ORGANIZATION*
- ```php artisan config:clear```
- ```composer install```
- ```npm install```
- ```npm run dev```
- open the page: *{host}/gpt-chat*

## More info
By default, the app uses "text-davinci-003" model, but you can redefine it in .env to another model, like this:
```
OPENAI_MODEL="gpt-3.5-turbo-0301"
```
You can get testing API key here https://platform.openai.com/account/api-keys . 
Also, OpenAI Organization key is available here: https://platform.openai.com/account/org-settings.
